import 'package:flutter/material.dart';
import 'package:test_task_for_creonit/pages/home.dart';
import 'package:test_task_for_creonit/product_categories.dart';

void main() async {
  await ProductsCategoriesSingleton().setProductList();
  runApp(MaterialApp(
    initialRoute: '/',
    routes: {
      '/': (context) => Home(),
    },
  ));
}
